const Actions = ({ actiontext, onClick = () => {} }) => {
  return (
    <button
      className="modal__actions__btn"
      onClick={() => {
        onClick();
      }}
    >
      {actiontext}
    </button>
  );
};

export default Actions;
